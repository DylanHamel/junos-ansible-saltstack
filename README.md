# Junos Automation with Ansible & Salt
###### Dylan Hamel - December 2019 - <dylan.hamel@protonmail.com>

Example of using Ansible and SaltStack to automation Juniper devices.
Lab is deployed on EVE-NG.



## Ansible

```shell
./bin/ansible-playbook -i hosts get_facts.yml --vault-password-file=.ansible_vault.key
```

#### 

#### Configure Syslog

With `juniper_junos_config`  you can define a state and any configuration not matching this state will be automatically deleted

XML data to configure Syslog host is :

```xml
<configuration>
  <system>
    <syslog replace="replace">
      <host>
        <name>{{ syslog_servers }}</name>
        <contents>
          <name>any</name>
          <any/>
        </contents>
      </host>
    </syslog>
  </system>
</configuration>
```

The actual configuration on Juniper device is :

```json
root# show system syslog
host 192.168.253.2 {
    any any;
}
host 1.1.1.1 {
    any any;
}
```

Run the playbook :

```shell
» ./bin/ansible-playbook -i hosts set_syslog_server.yml --vault-password-file=.ansible_vault.key

PLAY [Configure Syslog servers on Junos devices] *******************************************************************************

TASK [Configure Syslog servers] *******************************************************************************
Monday 30 December 2019  23:34:16 +0100 (0:00:00.071)       0:00:00.071 *******
changed: [p1]

TASK [Print Syslog configuration output] *******************************************************************************
Monday 30 December 2019  23:34:19 +0100 (0:00:03.093)       0:00:03.165 *******
ok: [p1] => {
    "junos_set_syslog.diff_lines": [
        "",
        "[edit system syslog]",
        "-    host 1.1.1.1 {",
        "-        any any;",
        "-    }"
    ]
}

PLAY RECAP *******************************************************************************
p1 : ok=2    changed=1  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0
```

Host `1.1.1.1` has been removed by the playbook and the play have `changed` the configuration.

The configuration is :

```json
root# show system syslog
host 192.168.253.2 {
    any any;
}
```

Ansible is **idempotent**, you can relaunch the playbook and nothing will happen

```shell
» ./bin/ansible-playbook -i hosts set_syslog_server.yml --vault-password-file=.ansible_vault.key

PLAY [Configure Syslog servers on Junos devices] *******************************************************************************

TASK [Configure Syslog servers] *******************************************************************************
Monday 30 December 2019  23:39:54 +0100 (0:00:00.061)       0:00:00.061 *******
ok: [p1]

TASK [Print Syslog configuration output] *******************************************************************************
Monday 30 December 2019  23:39:56 +0100 (0:00:02.466)       0:00:02.528 *******
ok: [p1] => {
    "junos_set_syslog.diff_lines": "VARIABLE IS NOT DEFINED!"
}

PLAY RECAP ********************************************************************************
p1  : ok=2    changed=0  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0
```

Configuration had not changed :smiley:

#### Ansible-Playbook [--check]

If you want check what configuration will be removed and added on the Junos devices, you can run the playbook with the `--check` options:

Junos device configuration before running the playbook :

```json
root# show system syslog
host 192.168.253.2 {
    any any;
}
```

Run the ansible-playbook :

```Shell
» ./bin/ansible-playbook -i hosts set_syslog_server.yml --vault-password-file=.ansible_vault.key --check
[WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details


PLAY [Configure Syslog servers on Junos devices] *******************************************************************************

TASK [Configure Syslog servers] *******************************************************************************
Tuesday 31 December 2019  00:07:16 +0100 (0:00:00.063)       0:00:00.063 ******
changed: [p1]

TASK [Print Syslog configuration output] *******************************************************************************
Tuesday 31 December 2019  00:07:18 +0100 (0:00:02.422)       0:00:02.486 ******
ok: [p1] => {
    "junos_set_syslog.diff_lines": [
        "",
        "[edit system syslog]",
        "+    host 192.168.253.200 {",
        "+        any any;",
        "+    }",
        "-    host 192.168.253.2 {",
        "-        any any;",
        "-    }"
    ]
}

PLAY RECAP ********************************************************************************
p1 : ok=2  changed=1  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0
```

You can see which configurtion should be remove and add on the devices but as you can see below the configuration on the Junos device has not changed !

```json
root# show system syslog
host 192.168.253.2 {
    any any;
}
```

