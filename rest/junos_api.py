#!./../bin/python

import requests
from requests.auth import HTTPBasicAuth
from lxml import etree

devices = {
    "10.0.4.11": {
        "username" : "root",
        "password" : "Jun1p3r",
        "http_port" : "8888"
    }
}

CONFIG_FILTER = """
<get-config>
    <source>
        <running/>
    </source>
    <filter type="subtree">
        <configuration>
            <system>
                <services/>
            </system>
        </configuration>
    </filter>
</get-config>"""

for device in devices:

    print(
        f"http://{device}:{devices.get(device).get('http_port', '3000')}/rpc")

    response = requests.post(
        url=f"http://{device}:{devices.get(device).get('http_port', '3000')}/rpc",
        data=CONFIG_FILTER,
        auth=HTTPBasicAuth(
            username=devices.get(device).get('username', 'admin'),
            password=devices.get(device).get('password', 'admin')
        ),
        headers={
            "Accept": "application/xml",
            "Content-Type": "application/xml"
        }
    )
    print(f"Response Status-Code: {response.status_code}")

    xml_lines = "\n".join(response.text.splitlines()[3:-1])
    #print(xml_lines)
    resp_xml = etree.fromstring(xml_lines)

    telnet_enabled = len(resp_xml.xpath("configuration/system/services/telnet"))  != 0
    ftp_enabled = len(resp_xml.xpath("configuration/system/services/ftp")) != 0

    if telnet_enabled:
        print(f"[{device}] - /!\ TELNET is activate")

    if ftp_enabled:
        print(f"[{device}] - /!\ FTP is activate")
    
    if ftp_enabled is False and telnet_enabled is False:
        print(f"[{device}] - Configuration is ok")
