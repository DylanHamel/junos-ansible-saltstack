#!./../bin/python
# Install : sudo pip install jsnapy
# Need to be run in root to avoid 'Permission denied' exception
#   > error: could not create '/etc/jsnapy': Permission denied


# set system services netconf
# commit

import xml.dom.minidom
import os
import pprint
from jnpr.junos import Device
from lxml import etree


PP = pprint.PrettyPrinter(indent=4)
rows, columns = os.popen('stty size', 'r').read().split()

def printline():
    print("*" * int(columns))

def test00(juniper_device: Device) -> None:
    PP.pprint(juniper_device.facts)

def test01(juniper_device:Device) -> None:
    print(juniper_device.cli("show route 0.0.0.0"))

def test02(juniper_device:Device) -> None:
    xml_object = juniper_device.rpc.get_route_information(table="inet.0")
    route_tables_lst = xml_object.findall(".//rt")
    for route_table in route_tables_lst:
        nexthops_lst = list()
        interfaces_out_lst = list()
        for nexthop in route_table.xpath("rt-entry/nh/to"):
            nexthops_lst.append(nexthop.text)
        for interface_out in route_table.xpath("rt-entry/nh/via"):
            interfaces_out_lst.append(interface_out.text)

        print(f"Route: {route_table.findtext('rt-destination').strip()}")
        print(f"Next-Hop: {nexthops_lst}")
        print(f"Interfaces Out: {interfaces_out_lst}")
        print(
            f"Protocols: {route_table.findtext('rt-entry/protocol-name').strip()}")
        print("\t\t-----")


def test03(juniper_device: Device) -> None:
    ## Get all Routing table
    xml_object = juniper_device.rpc.get_route_information()
    routing_tables_lst = xml_object.findall(".//route-table")
    i = 1
    for routing_table in routing_tables_lst:
        print(f"Routing table name [{i}]: {routing_table.findtext('table-name')}")
        i += 1


def test04(juniper_device: Device) -> None:
    ## Get all config
    xml_config = juniper_device.rpc.get_config(
        filter_xml=etree.XML('<configuration><system/></configuration>')
        #options={"format": "json"}
    )
    print(etree.tostring(xml_config))


def test05(juniper_device: Device) -> None:
    neighbor_state_lst = list()
    xml_object = juniper_device.rpc.get_ospf_neighbor_information()
    neighbor_state = xml_object.xpath('ospf-neighbor/ospf-neighbor-state')
    
    for neighbor in neighbor_state:
        neighbor_state_lst.append(neighbor.text)

    print(neighbor_state_lst)
        

def test06(juniper_device: Device) -> None:
    xml_object = juniper_device.rpc.get_ospf_neighbor_information()
    etree.dump(xml_object)
    #
    # TO PRINT XML OUTPUT !!!!! 

# with juniper_device = Device(host="10.0.4.11",user="root",passwd="Jun1p3r") as device:
#   PP.pprint(juniper_device.facts)

juniper_device = Device(
    host="10.0.4.11",
    user="root",
    passwd="Jun1p3r"
)
juniper_device.open()

#printline()
#test00(juniper_device)
#printline()
#test01(juniper_device)
#printline()
#test02(juniper_device)
#printline()   
#test03(juniper_device)
#printline()
#test04(juniper_device)
#printline()
test05(juniper_device)
printline()
test06(juniper_device)
printline()

juniper_device.close()
print("Program finished ...")
