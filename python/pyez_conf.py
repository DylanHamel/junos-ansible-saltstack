#!./../bin/python

import os
import pprint
from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.utils.sw import SW # Software utilities
from jnpr.junos.exception import *

PP = pprint.PrettyPrinter(indent=4)
rows, columns = os.popen('stty size', 'r').read().split()

def printline():
    print("*" * int(columns))


def fct_config(juniper_device: Device) -> None:
    try:
        conf = Config(juniper_device)
        data = """
            system {
                services {
                    rest {
                        http {
                            port 8888;
                        }
                    }
                }
            }"""

        conf.lock()
        conf.load(data, format='text')
        conf.pdiff()
        if conf.commit_check():
            print("COMMIT")
            conf.commit()
        else:
            print("ROLLBACK")
            conf.rollback()
        conf.unlock()
    except ConnectAuthError as e:
        print("Authentification error")
    except ConnectTimeoutError as e:
        print("NETCONF connection timed out!")
    except ConnectError as e:
        print(f"Connection error : {e}")
    except ConfigLoadError as e:
        print("Failure when loading a configuration")
    except Exception as e:
        print(f"Some other exception happened: {e}")
    #finally:
    #    dev.close()
    

with Device(host="10.0.4.11", user="root", passwd="Jun1p3r") as device:
    pass
    #fct_config(juniper_device=device)


with Device(host="10.0.4.11", user="root", passwd="Jun1p3r") as device:
    with Config(device, mode='exclusive') as conf:
        conf.load("set system services rest http port 8888", format='set')
        diff = conf.diff()
        if diff is None:
            print("Configuration is already up to date")
        else:
            print(diff)
            if conf.commit_check():
                conf.commit()
            else:
                conf.rollback()

def progress_callback(dev, report):
    print(report)

with Device(host="10.0.4.11", user="root", passwd="Jun1p3r") as device:
    with SW(device, mode='exclusive') as sw:
        package = '/var/tmp/junos-openconfig-x86.tgz'
        ok = sw.install(
            package=package,
            no_copy=True,
            validate=False,
            progress=progress_callback
        )
        sw.reboot()