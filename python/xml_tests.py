#!./../bin/python

from lxml import etree
import pprint
PP = pprint.PrettyPrinter(indent=4)

CONF_FILENAME = "interfaces.xml"

config = etree.parse(CONF_FILENAME).getroot()

#print(config)
# <Element rpc-reply at 0x10e06c3c8>

interface_name_lst = config.xpath("configuration/interfaces/interface/name")
# print(interface_name_lst)
# [<Element name at 0x108e033c8>, <Element name at 0x108e034c8>, <Element name at 0x108e03508>, 
# <Element name at 0x108e03548>, <Element name at 0x108e03588>, <Element name at 0x108e03608>]

interface_name_set = set()
interface_name_ip_dict = dict()

for interface_name in interface_name_lst:

    # Add Interface name into a SET (useless)
    interface_name_set.add(interface_name.text)
    interface_name_ip_dict[interface_name.text] = list()

    # Retrieve a list containing IPv4 addresses of the interface
    interfaces_ip = config.xpath(
        f"configuration/interfaces/interface[./name='{interface_name.text}']/unit/family/inet/address/name")

    for interface_ip in interfaces_ip:
        interface_name_ip_dict[interface_name.text].append(interface_ip.text)


print(f"Interfaces configured are : ")
PP.pprint(interface_name_ip_dict)
print("Program finished ...")
