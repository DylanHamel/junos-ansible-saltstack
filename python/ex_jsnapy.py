#!./../bin/python
# Install : sudo pip install jsnapy
# Need to be run in root to avoid 'Permission denied' exception
#   > error: could not create '/etc/jsnapy': Permission denied


from jnpr.junos import Device
from jnpr.jsnapy import SnapAdmin

import pprint
PP = pprint.PrettyPrinter(indent=4)

with open("./jsnapy_config/config.yml", "r") as f:
    jsnapy_config = f.read()

js = SnapAdmin()
dev = Device(
    host="172.16.194.2",
    user="root",
    passwd="Admin123"
)

dev.open()
js.snap(jsnapy_config, 'py_pre')
PP.pprint(dev.facts)
PP.pprint(dev.rpc.get_bgp_neighbor_information())
js.snap(jsnapy_config, 'py_post')
snapcheck = js.check(
    jsnapy_config, 
    pre_file='py_pre', 
    post_file='py_post'
)
dev.close()
