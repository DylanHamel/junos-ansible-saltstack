#!./../bin/python
from jnpr.junos.exception import *
from jnpr.junos import Device


dev = Device(
    host="172.16.194.2",
    user="root",
    passwd="Admin123"
)

try:
    dev.open()
    print(dev.facts)
except ConnectAuthError as e:
    print(e)
    print("Error, please check username / password.")
except ConnectTimeoutError as e:
    print(e)
    print("Error, please check the IP address and port.")
except ConnectRefusedError as e:
    print(e)
    print("Error, please check that the service is listening.")
except ConnectError as e:
    print(e)
    print("Catch by ConnectError ...")
finally:
    dev.close()