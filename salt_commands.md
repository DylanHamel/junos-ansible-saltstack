See Keys :
```shell
$ salt-key -c ./etc/salt -L
Accepted Keys:
Denied Keys:
Unaccepted Keys:
Rejected Keys:
```


Add key:
```shell
# -A, --accept-all    Accept all pending keys.
$ salt-key -c ./etc/salt -A          
The following keys are going to be accepted:
Unaccepted Keys:
dev05
Proceed? [n/Y] Y
Key for minion dev05 accepted.

##############################################
# Key for dev05 is now accepted :)
##############################################
$ salt-key -c ./etc/salt -L
Accepted Keys:
dev05
Denied Keys:
Unaccepted Keys:
Rejected Keys:

```

Test connection with the minion
```shell
$ salt -c etc/salt/ '*' test.version
dev05:
    2019.2.2
```

Test connection with the minion (2)
```shell
$ salt -c etc/salt/ 'dev*' test.ping   
dev05:
    True
```

Execute a shell command on the remote host :
```shell
$ salt -c etc/salt/ 'dev*' cmd.run "uname -a"
dev05:
    Linux dev05 3.10.0-862.3.3.el7.x86_64 #1 SMP Wed Jun 13 05:44:23 EDT 2018 x86_64 x86_64 x86_64 GNU/Linux
```

Create a file with content on the remote host (minion) :
```shell
$ salt -c etc/salt/ 'dev*' file.write '/tmp/salt_file_test.yml' "hostname: dev05"           
dev05:
    Wrote 1 lines to "/tmp/salt_file_test.yml"
```

It is possible to copy a file from the master to the minion.
For that the minion must have access to the master's folder...
If this access is not monitored and defined, it can be a big fail of security !
In the master configuration, you can define which folder can be accceded by minions.
```shell
 file_roots:
    base:
      - /srv/salt/
    dev:
      - /srv/salt/dev/services
      - /srv/salt/dev/states
    prod:
      - /srv/salt/prod/services
      - /srv/salt/prod/states
 
 file_roots:
   base:
     - /srv/salt
```

```shell
$ salt -c etc/salt/ 'dev05' cp.get_file 'salt://salt.txt' '/tmp/salt/copy_file.txt'
```
